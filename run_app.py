#!/usr/bin/env python
# -*- encoding:utf-8 -*-
from app import app
import os

if __name__ == "__main__":
    app.run("0.0.0.0", debug=True, port=os.getenv("BACKEND_PORT"))
