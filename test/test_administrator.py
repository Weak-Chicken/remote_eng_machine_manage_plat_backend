import pytest
import requests
import os

__URL__ = "http://101.132.152.202:8080"
__URL_PREFIX__ = "administrator"

class TestAdministrator():
    def test_select_all(self):
        url = f"{__URL__}/{__URL_PREFIX__}/all"
        log_path = f"./test_res/{__URL_PREFIX__}"
        log_file_name = "select_all.log"

        resp = requests.get(url=url)

        assert resp.status_code == 200
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        with open(f"{log_path}/{log_file_name}", "w+") as f:
            f.write(resp.text)
    
    def test_select_by_ids(self):
        url = f"{__URL__}/{__URL_PREFIX__}/ids"
        data = {"administrator_ids": ["1", "2", "999"]}
        log_path = f"./test_res/{__URL_PREFIX__}"
        log_file_name = "select_by_ids.log"

        resp = requests.post(url=url, data=data)
        print(resp.text)

        assert resp.status_code == 200
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        with open(f"{log_path}/{log_file_name}", "w+") as f:
            f.write(resp.text)
