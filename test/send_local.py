import pytest
import requests
import os
import json
from test import __TEST_URL__

__URL__ = __TEST_URL__
__URL_PREFIX__ = "administrator"

class TestAdministrator():
    def test_select_all(self):
        url = f"{__URL__}/{__URL_PREFIX__}/all"
        log_path = f"./test_res/{__URL_PREFIX__}"
        log_file_name = "select_all.log"

        resp = requests.get(url=url)

        return resp.text
    
    def test_select_by_ids(self):
        url = f"{__URL__}/{__URL_PREFIX__}/ids"
        data = {"administrator_ids": ["1", "2", "999"]}

        data = json.dumps(data)

        resp = requests.post(url, json=data)
        return resp.text

    def test_updates(self):
        url = f"{__URL__}/{__URL_PREFIX__}/updates"
        data = {"administrator_infos": [{"id": 222, "name": "test_up", "deptId": 2, "salary": 2}, {"id": 333, "name": "1234", "deptId": 3, "salary": 3}]}

        data = json.dumps(data)
        resp = requests.put(url, json=data)
        return resp.text

if __name__ == "__main__":
    test = TestAdministrator()
    test_method = test.test_updates()
    print(test_method, type(test_method))
