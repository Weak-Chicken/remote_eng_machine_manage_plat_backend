#!/bin/bash
# cd "$(dirname "$0")"

export STATUS=dev
# export STATUS=test
# export STATUS=prod

case "$STATUS" in
    [dD][eE][vV]) 
        source ./env_building/env_vars.dev
        ;;
    [tT][eE][sS][tT]) 
        source ./env_building/env_vars.test
        ;;
    [pP][rR][oO][dD]) 
        source ./env_building/env_vars.prod
        ;;
    *)
        echo "WRONG STATUS value!"
        ;;
esac
