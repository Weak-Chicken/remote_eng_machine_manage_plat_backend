#!/bin/bash
cd "$(dirname "$0")" && cd "../../"

source ./env_building/env_vars.sh

docker-compose -f ./env_building/docker-compose.prod.yml up -d --build
