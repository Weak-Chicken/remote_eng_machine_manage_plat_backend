#!/bin/bash
cd "$(dirname "$0")" && cd "../../"

source ./env_building/env_vars.sh

docker build --no-cache -t ${CACHE_IMAGE_NAME} --build-arg SELECT_IMAGE=${SELECT_IMAGE} -f ./env_building/${CACHE_ENV_BUILD_DOCKERFILE} .
