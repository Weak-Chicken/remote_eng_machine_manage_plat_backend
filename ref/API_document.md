## V1

##### URL: /apis/v1/

---

### /administrator

#### GET /administrator/all
administrator get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): {
>     "SANum": user's id,
>     "Name": "user's name",
>   }
> }
> ```

#### POST /administrator/ids
administrator get by ids
##### input
> ```json
> {
>   "administrator_ids": ["administrator's id", "administrator's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>         "SANum": user's id,
>         "Name": "user's name",
>       },
>       {
>         "SANum": user's id,
>         "Name": "user's name",
>       }
>       ...
>   ]
> }
> ```

#### POST /administrator/adds
add some administrators by list
##### input
> ```json
> {
>   "administrator_infos": [
>       {
>           "Name": "user's name",
>           "Psw": "user's password"
>       },
>       {
>           "Name": "user's name",
>           "Psw": "user's password"
>       }
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>         "SANum": user's id,
>         "Name": "user's name",
>       },
>       {
>         "SANum": user's id,
>         "Name": "user's name",
>       }
>       ...
>   ]
> }
> ```

#### PUT /administrator/updates
add some administrators by list
##### input
> ```json
> {
>   "administrator_infos": [
>       {
>           "SANum": user's id,
>           "Name": "user's name",
>           "Psw": "user's password"
>       },
>       {
>           "SANum": user's id,
>           "Name": "user's name",
>           "Psw": "user's password"
>       }
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>         "SANum": user's id,
>         "Name": "user's name",
>       },
>       {
>         "SANum": user's id,
>         "Name": "user's name",
>       }
>       ...
>   ]
> }
> ```

#### DELETE /administrator/deletes
delete some administrators by list
##### input
> ```json
> {
>   "administrator_ids": ["administrator's id", "administrator's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /user

#### GET /user/all
user get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): {
>       "IdCard": "user's id",
>       "Name": "user's name",
>       "Sex": "user's sex",
>       "Phone": "user's phone",
>       "status_code": true or false,
>   }
> }
> ```

#### POST /user/ids
user get by ids
##### input
> ```json
> {
>   "user_ids": ["user's id", "user's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "status_code": true or false,
>       },
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "status_code": true or false,
>       }
>       ...
>   ]
> ```

#### POST /user/adds
add some users by list
##### input
> ```json
> {
>   "user_infos": [
>       {
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "Psw" : "user's password",
>       },
>       {
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "Psw" : "user's password",
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "status_code": true or false,
>       },
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "status_code": true or false,
>       }
>       ...
>   ]
> }
> ```

#### PUT /user/updates
add some users by list
##### input
> ```json
> {
>   "administrator_infos": [
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "Psw" : "user's password",
>           "status_code": true or false,
>       },
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "Psw" : "user's password",
>           "status_code": true or false,
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "status_code": true or false,
>       },
>       {
>           "IdCard": "user's id",
>           "Name": "user's name",
>           "Sex": "user's sex",
>           "Phone": "user's phone",
>           "status_code": true or false,
>       }
>       ...
>   ]
> }
> ```

#### DELETE /user/deletes
delete some users by list
##### input
> ```json
> {
>   "user_ids": ["user's id", "user's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /operator

#### GET /operator/all
operator get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): {
>       "IdCard": "operator's id",
>       "Name": "operator's name",
>       "Sex": "operator's sex",
>       "Phone": "operator's phone"
>   }
> }
> ```

#### POST /operator/ids
operator get by ids
##### input
> ```json
> {
>   "operator_ids": ["operator's id", "operator's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       },
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       }
>       ...
>   ]
> }
> ```

#### POST /operator/adds
add some operators by list
##### input
> ```json
> {
>   "operator_infos": [
>       {
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       },
>       {
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       }
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       },
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       }
>       ...
>   ]
> }
> ```

#### PUT /operator/updates
add some operators by list
##### input
> ```json
> {
>   "administrator_infos": [
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       },
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       }
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "userData" (if success): [
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       },
>       {
>           "IdCard": "operator's id",
>           "Name": "operator's name",
>           "Sex": "operator's sex",
>           "Phone": "operator's phone"
>       }
>       ...
>   ]
> }
> ```

#### DELETE /operator/deletes
delete some operators by list
##### input
> ```json
> {
>   "operator_ids": ["operator's id", "operator's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /project

#### GET /project/all
project get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "projectData" (if success): {
>       "IdCard": "project id",
>       "Name": "project name",
>       "Number": "project number",
>       "PM": "project manager id",
>       "Member": "project member",
>       "Location": "project location",
>       "Status": "project status",
>       "AS": "project AS",
>       "PF": "project PF",
>   }
> }
> ```

#### POST /project/ids
project get by ids
##### input
> ```json
> {
>   "project_ids": ["project's id", "project's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "projectData" (if success): [
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       },
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       }
>       ...
>   ]
> }
> ```

#### POST /project/adds
add some projects by list
##### input
> ```json
> {
>   "project_infos": [
>       {
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       },
>       {
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       }
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "projectData" (if success): [
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       },
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       }
>       ...
>   ]
> }
> ```

#### PUT /project/updates
add some projects by list
##### input
> ```json
> {
>   "project_infos": [
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       },
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       }
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "projectData" (if success): [
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       },
>       {
>           "IdCard": "project id",
>           "Name": "project name",
>           "Number": "project number",
>           "PM": "project manager id",
>           "Member": "project member",
>           "Location": "project location",
>           "Status": "project status",
>           "AS": "project AS",
>           "PF": "project PF",
>       }
>       ...
>   ]
> }
> ```

#### DELETE /administrator/deletes
delete some administrators by list
##### input
> ```json
> {
>   "project_ids": ["project's id", "project's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /machine

#### GET /machine/all
machine get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineData" (if success): {
>       "IdCard": "IdCard",
>       "DeviceNum": "DeviceNum",
>       "Type": "Type",
>       "EngineNumber": "EngineNumber",
>       "EnginePower": (int)EnginePower,
>       "Dimensions": "Dimensions",
>       "Quality": (int)Quality,
>       "Life": (int)Life,
>       "WorkingTime": (int)WorkingTime,
>       "Manufacturer": "Manufacturer",
>       "Nameplate": "Nameplate",
>       "Purchase_date": (float)Purchase_date,
>       "Purchase_price": (float)Purchase_price,
>       "Tank_volume": (float)Tank_volume,
>       "SOC": (float)SOC,
>       "ADR": (float)ADR
>   }
> }
> ```

#### POST /machine/ids
machine get by ids
##### input
> ```json
> {
>   "machine_ids": ["machine's id", "machine's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineData" (if success): [
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>        },
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>       },
>       ...
>   ]
> }
> ```

#### POST /machine/adds
add some machines by list
##### input
> ```json
> {
>   "machine_infos": [
>        {
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>        },
>        {
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineData" (if success): [
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>        },
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>       },
>       ...
>   ]
> }
> ```

#### PUT /machine/updates
add some machines by list
##### input
> ```json
> {
>   "machine_infos": [
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>        },
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineData" (if success): [
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>        },
>        {
>           "IdCard": "IdCard",
>           "DeviceNum": "DeviceNum",
>           "Type": "Type",
>           "EngineNumber": "EngineNumber",
>           "EnginePower": (int)EnginePower,
>           "Dimensions": "Dimensions",
>           "Quality": (int)Quality,
>           "Life": (int)Life,
>           "WorkingTime": (int)WorkingTime,
>           "Manufacturer": "Manufacturer",
>           "Nameplate": "Nameplate",
>           "Purchase_date": (float)Purchase_date,
>           "Purchase_price": (float)Purchase_price,
>           "Tank_volume": (float)Tank_volume,
>           "SOC": (float)SOC,
>           "ADR": (float)ADR
>       },
>       ...
>   ]
> }
> ```

#### DELETE /machine/deletes
delete some machines by list
##### input
> ```json
> {
>   "machine_ids": ["machine's id", "machine's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /machine_status

#### GET /machine_status/all
machine_status get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineStatusData" (if success): {
>       "IdCard": "IdCard",
>       "Machine": "Machine",
>       "Status": "Status",
>       "Type": "Type",
>       "InstallationTime": "InstallationTime"
>   }
> }
> ```

#### POST /machine_status/ids
machine_status get by ids
##### input
> ```json
> {
>   "machine_status_ids": ["machine_status's id", "machine_status's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineStatusData" (if success): [
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       ...
>   ]
> }
> ```

#### POST /machine_status/adds
add some machine_status by list
##### input
> ```json
> {
>   "machine_status_infos": [
>       {
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       {
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineStatusData" (if success): [
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       ...
>   ]
> }
> ```

#### PUT /machine_status/updates
add some machine_status by list
##### input
> ```json
> {
>   "machine_status_infos": [
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "machineStatusData" (if success): [
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       {
>           "IdCard": "IdCard",
>           "Machine": "Machine",
>           "Status": "Status",
>           "Type": "Type",
>           "InstallationTime": "InstallationTime"
>       },
>       ...
>   ]
> }
> ```

#### DELETE /machine_status/deletes
delete some machine_status by list
##### input
> ```json
> {
>   "machine_status_ids": ["machine_status's id", "machine_status's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /machine_op_1

#### GET /machine_op_1/all
machine_op_1 get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): {
>       "DeviceNum": "DeviceNum",
>       "CommunityNum": "CommunityNum",
>       "DeviceStatus": "DeviceStatus"
>   }
> }
> ```

#### POST /machine_op_1/ids
machine_op_1 get by ids
##### input
> ```json
> {
>   "machine_op_1_ids": ["machine_op_1's id", "machine_op_1's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### POST /machine_op_1/adds
add some machine_op_1 by list
##### input
> ```json
> {
>   "machine_op_1_infos": [
>       {
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### PUT /machine_op_1/updates
add some machine_op_1 by list
##### input
> ```json
> {
>   "machine_op_1_infos": [
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "CommunityNum": "CommunityNum",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### DELETE /machine_op_1/deletes
delete some machine_op_1 by list
##### input
> ```json
> {
>   "machine_op_1_ids": ["machine_op_1's id", "machine_op_1's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /machine_op_2

#### GET /machine_op_2/all
machine_op_2 get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): {
>       "DeviceNum": "DeviceNum",
>       "DeviceType": "DeviceType",
>       "DeviceStatus": "DeviceStatus"
>   }
> }
> ```

#### POST /machine_op_2/ids
machine_op_2 get by ids
##### input
> ```json
> {
>   "machine_op_2_ids": ["machine_op_2's id", "machine_op_2's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### POST /machine_op_2/adds
add some machine_op_2 by list
##### input
> ```json
> {
>   "machine_op_2_infos": [
>       {
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### PUT /machine_op_2/updates
add some machine_op_2 by list
##### input
> ```json
> {
>   "machine_op_2_infos": [
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "DeviceType": "DeviceType",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### DELETE /machine_op_2/deletes
delete some machine_op_2 by list
##### input
> ```json
> {
>   "machine_op_2_ids": ["machine_op_2's id", "machine_op_2's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /machine_op_3

#### GET /machine_op_3/all
machine_op_3 get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): {
>       "DeviceNum": "DeviceNum",
>       "Fuel": "Fuel",
>       "DeviceStatus": "DeviceStatus"
>   }
> }
> ```

#### POST /machine_op_3/ids
machine_op_3 get by ids
##### input
> ```json
> {
>   "machine_op_3_ids": ["machine_op_3's id", "machine_op_3's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### POST /machine_op_3/adds
add some machine_op_3 by list
##### input
> ```json
> {
>   "machine_op_3_infos": [
>       {
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### PUT /machine_op_3/updates
add some machine_op_3 by list
##### input
> ```json
> {
>   "machine_op_3_infos": [
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Fuel": "Fuel",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### DELETE /machine_op_3/deletes
delete some machine_op_3 by list
##### input
> ```json
> {
>   "machine_op_3_ids": ["machine_op_3's id", "machine_op_3's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```

---

### /machine_op_4

#### GET /machine_op_4/all
machine_op_4 get all
##### input
> ```json
> {
>   (NO INPUT NEEDED)
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): {
>       "DeviceNum": "DeviceNum",
>       "Message": "Message",
>       "DeviceStatus": "DeviceStatus"
>   }
> }
> ```

#### POST /machine_op_4/ids
machine_op_4 get by ids
##### input
> ```json
> {
>   "machine_op_4_ids": ["machine_op_4's id", "machine_op_4's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### POST /machine_op_4/adds
add some machine_op_4 by list
##### input
> ```json
> {
>   "machine_op_4_infos": [
>       {
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### PUT /machine_op_4/updates
add some machine_op_4 by list
##### input
> ```json
> {
>   "machine_op_4_infos": [
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code,
>   "opData" (if success): [
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       {
>           "DeviceNum": "DeviceNum",
>           "Message": "Message",
>           "DeviceStatus": "DeviceStatus"
>       },
>       ...
>   ]
> }
> ```

#### DELETE /machine_op_4/deletes
delete some machine_op_4 by list
##### input
> ```json
> {
>   "machine_op_4_ids": ["machine_op_4's id", "machine_op_4's id"]
> }
> ```

##### output
> ```json
> {
>   "success": true or false,
>   "status_code": status_code
> ```
