USE emmp;

CREATE TABLE test_tb(
    id INT(11),
    name VARCHAR(25),
    deptId INT(11),
    salary FLOAT
);

INSERT INTO test_tb(
    id, name, deptId, salary
) VALUES (
    4, '4', 66354, 6656.06
);

/*
MySQLdb._exceptions.OperationalError: (2059, "Authentication plugin 'caching_sha2_password' cannot be loaded: /usr/lib64/mysql/plugin/caching_sha2_password.so: cannot open shared object file: No such file or directory")
*/
ALTER USER 'yourusername'@'%' IDENTIFIED WITH mysql_native_password BY 'youpassword';


select table_name from information_schema.tables where table_schema='emmp';

SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'emmp' AND TABLE_NAME = 'machine_op_2';

/* ============================================= */

CREATE TABLE administrator(
    SANum varchar(32) PRIMARY KEY,
    Name VARCHAR(30) NOT NULL,
    Psw VARCHAR(32) NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE user(
    IdCard VARCHAR(18) PRIMARY KEY,
    Name VARCHAR(30) NOT NULL,
    Sex VARCHAR(5) NOT NULL,
    Phone VARCHAR(20) NOT NULL,
    Psw VARCHAR(32) NOT NULL,
    Flag BOOLEAN NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE operator(
    IdCard VARCHAR(18) PRIMARY KEY,
    Name VARCHAR(30) NOT NULL,
    Sex VARCHAR(5) NOT NULL,
    Phone VARCHAR(20) NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE project(
    IdCard varchar(18) PRIMARY KEY,
    Name varchar(30) NOT NULL,
    Number varchar(30) NOT NULL,
    PM varchar(30) NOT NULL,
    Member varchar(30),
    Location varchar(20),
    Status varchar(5),
    TAS datetime,
    PF datetime,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE machine(
    IdCard varchar(18) PRIMARY KEY,
    DeviceNum varchar(18),
    Type varchar(18) NOT NULL,
    EngineNumber varchar(30) NOT NULL,
    EnginePower Int(32),
    Dimensions varchar(50),
    Quality Int(32),
    Life Int(32),
    WorkingTime Int(32),
    Manufacturer varchar(30),
    Nameplate varchar(30) NOT NULL,
    Purchase_date varchar(30),
    Purchase_price Float(32),
    Tank_volume Float(32) NOT NULL,
    SOC Float(32) NOT NULL,
    ADR Float(32) NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE machine_status(
    IdCard varchar(18) PRIMARY KEY,
    Machine varchar(18),
    Status varchar(10) NOT NULL,
    Type varchar(10) NOT NULL,
    InstallationTime datetime NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE machine_op_1(
    DeviceNum varchar(20) PRIMARY KEY,
    CommunityNum Int(32),
    DeviceStatus datetime NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE machine_op_2(
    DeviceNum varchar(20) PRIMARY KEY,
    DeviceType varchar(25),
    DeviceStatus datetime NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE machine_op_3(
    DeviceNum varchar(20) PRIMARY KEY,
    Fuel Float(32)  NOT NULL,
    DeviceStatus datetime NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);

CREATE TABLE machine_op_4(
    DeviceNum varchar(20) PRIMARY KEY,
    Message TEXT,
    DeviceStatus datetime NOT NULL,
    Reserve_1 TEXT,
    Reserve_2 TEXT,
    Reserve_3 TEXT
);
