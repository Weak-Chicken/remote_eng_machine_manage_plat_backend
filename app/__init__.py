from flask import Flask
from flask_mysqldb import MySQL

app = Flask(__name__, static_folder='./static')

app.config['MYSQL_HOST'] = 'db'
app.config['MYSQL_USER'] = 'emmp'
app.config['MYSQL_PASSWORD'] = 'PassW0rd!Emmp'
app.config['MYSQL_DB'] = 'emmp'
app.config['SELF_IP'] = 'http://101.132.152.202:8080'
app.config['SELF_IP'] = 'http://192.168.1.100:8080'

mysql = MySQL(app)

from app import views
from app.api.v1 import administrator
from app.api.v1 import user
from app.api.v1 import operator
from app.api.v1 import project
from app.api.v1 import machine
from app.api.v1 import machine_status
from app.api.v1 import machine_op_1
from app.api.v1 import machine_op_2
from app.api.v1 import machine_op_3
from app.api.v1 import machine_op_4