from app import app
from app import mysql
from flask import request, render_template, make_response, jsonify
import requests
from app.api.v1 import basic_db_ops, MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB, SELF_IP
from app.iot import v1
import traceback
import json
import copy
import random
from app.api.v1.basic_db_ops import concat_add_info, hide_some_key, update_str_constractor, constract_result

sub_url = '/project'
# table_head = '(SANum, Name, Psw, Reserve_1, Reserve_2, Reserve_3)'
table_head = ['IdCard', 'Name', 'Number', 'PM', 'Member', 'Location', 'Status', 'TAS', 'PF']
primary_key = 'IdCard'
primary_key_len = 18
# hide_key = ['Psw', 'Reserve_1', 'Reserve_2', 'Reserve_3']
hide_key = []
table_name = "project"

@app.route(sub_url + '/adds', methods=['POST'])
def project_add_single():
    if request.method == 'POST':
        infos = request.get_json()["project_infos"]
    
        db = mysql.connection
        cursor = db.cursor()
        return_list = []

        try:
            for info in infos:
                # init pk
                tmp_info_pk = ""
                for i in range(primary_key_len):
                    tmp_info_pk += str(random.randint(1, 9))
                info[primary_key] = tmp_info_pk

                cursor.execute(basic_db_ops.select_by_ids(table_name, [info[primary_key]], primary_key))
                is_repeat = cursor.fetchall()
                
                # verify pk
                while len(is_repeat) != 0:
                    tmp_info_pk = ""
                    for i in range(primary_key_len):
                        tmp_info_pk += str(random.randint(1, 9))
                    info[primary_key] = tmp_info_pk

                    cursor.execute(basic_db_ops.select_by_ids(table_name, [info[primary_key]], primary_key))
                    is_repeat = cursor.fetchall()

                # add
                add_infos = concat_add_info(table_head, info)

                sql = basic_db_ops.add_single(table_name, str(table_head).replace('[', '(').replace(']', ')').replace('\'', ''), add_infos)

                cursor.execute(sql)
                results = cursor.fetchall()
                db.commit()
                return_list.append(tmp_info_pk)

            # select it again
            sql = basic_db_ops.select_by_ids(table_name, return_list, primary_key, hide_some_key(table_head, hide_key))
            cursor.execute(sql)
            results = cursor.fetchall()

            return make_response(jsonify({
                "status_code": 200,
                "success": True,
                "userData": constract_result(results, hide_some_key(table_head, hide_key, parse_for_db=False))
            }), 200)
        except:
            db.rollback()
            return make_response(jsonify({
                "status_code": 500,
                "success": False,
                "result": traceback.format_exc()
            }), 500)

@app.route(sub_url + '/all', methods=['GET'])
def project_select_all():
    db = mysql.connection
    cursor = db.cursor()
    sql = basic_db_ops.select_all(table_name, hide_some_key(table_head, hide_key))
    try:
        cursor.execute(sql)
        results = cursor.fetchall()
        return make_response(jsonify({
            "status_code": 200,
            "success": True,
            "result": constract_result(results, hide_some_key(table_head, hide_key, parse_for_db=False))
        }), 200)
    except:
        return make_response(jsonify({
            "status_code": 500,
            "success": False,
            "result": traceback.format_exc()
        }), 500)

@app.route(sub_url + '/ids', methods=['POST'])
def project_select_by_ids():
    if request.method == 'POST':
        ids = request.get_json()['project_ids']

        db = mysql.connection
        cursor = db.cursor()
        sql = basic_db_ops.select_by_ids(table_name, ids, primary_key, hide_some_key(table_head, hide_key))
        try:
            cursor.execute(sql)
            results = cursor.fetchall()
            return make_response(jsonify({
                "status_code": 200,
                "success": True,
                "result": constract_result(results, hide_some_key(table_head, hide_key, parse_for_db=False))
            }), 200)
        except:
            return make_response(jsonify({
                "status_code": 500,
                "success": False,
                "result": traceback.format_exc()
            }), 500)

@app.route(sub_url + '/updates', methods=['PUT'])
def project_updates():
    if request.method == 'PUT':
        infos = request.get_json()["project_infos"]

        db = mysql.connection
        cursor = db.cursor()
        updated_ids = []

        try:
            for info in infos:
                # update it one by one
                update_infos = update_str_constractor(table_head, info)

                sql = basic_db_ops.update_single(table_name, update_infos, info[primary_key], primary_key)

                cursor.execute(sql)
                results = cursor.fetchall()
                db.commit()
                updated_ids.append(info[primary_key])

            # select it again
            sql = basic_db_ops.select_by_ids(table_name, updated_ids, primary_key, hide_some_key(table_head, hide_key))
            cursor.execute(sql)
            results = cursor.fetchall()

            return make_response(jsonify({
                "status_code": 200,
                "success": True,
                "result": constract_result(results, hide_some_key(table_head, hide_key, parse_for_db=False))
            }), 200)
        except:
            db.rollback()
            return make_response(jsonify({
                "status_code": 500,
                "success": False,
                "result": traceback.format_exc()
            }), 500)

@app.route(sub_url + '/deletes', methods=['DELETE'])
def project_delete_by_list():
    if request.method == 'DELETE':
        ids = request.get_json()["project_ids"]

        db = mysql.connection
        cursor = db.cursor()
        sql = basic_db_ops.delete_by_list(table_name, ids, primary_key)

        try:
            cursor.execute(sql)
            db.commit()
            return make_response(jsonify({
                "status_code": 200,
                "success": True,
            }), 200)
        except:
            db.rollback()
            return make_response(jsonify({
                "status_code": 500,
                "success": False,
                "result": traceback.format_exc()
            }), 500)
