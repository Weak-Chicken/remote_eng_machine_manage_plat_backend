from app import app
from app import mysql
import copy

def select_by_ids(table_name: str, ids: list, id_key: str, select_key: str="*"):
    sql_statement = f"select {select_key} from {table_name} where {id_key} in {str(ids).replace('[', '(').replace(']', ')')}"
    return sql_statement

def select_all(table_name: str, select_key: str="*"):
    sql_statement = f"select {select_key} from {table_name}"
    return sql_statement

def update_single(table_name: str, update_infos: list, uid: str, uid_key:str):
    """[summary]

    :param table_name: [description]
    :type table_name: str
    :param update_infos: [description]
    :type update_infos: list
    :param uid: [description]
    :type uid: str
    :param uid_key: [description]
    :type uid_key: str
    """
    set_str = "set "
    for info in update_infos:
        set_str += f"{info[0]} = {info[1]},"
    set_str = set_str[:-1]
    sql_statement = f"update {table_name} {set_str} where {uid_key}='{uid}'"
    return sql_statement

def add_single(table_name: str, table_head:str, add_infos: str):
    sql_statement = f"insert into {table_name}{table_head} values {add_infos}"
    return sql_statement

def delete_by_list(table_name: str, ids: list, id_key: str):
    sql_statement = f"delete from {table_name} where {id_key} in  {str(ids).replace('[', '(').replace(']', ')')}"
    return sql_statement

def concat_add_info(headers: list, info: dict):
    res = "("
    for header in headers:
        if type(info[header]) == str:
            res += f"'{info[header]}', "
        else:
            res += f"{info[header]}, "
    res = res[:-2]
    return res + ")"

def hide_some_key(headers: list, headers_to_hide: list, parse_for_db: bool=True):
    tmp_header = copy.deepcopy(headers)
    for header in headers_to_hide:
        tmp_header.remove(header)
    if parse_for_db:
        tmp_header = str(tmp_header).replace('[', '').replace(']', '').replace('\'', '')
    return tmp_header

def update_str_constractor(headers: list, info: dict):
    res = []
    for header in headers:
        if type(info[header]) == str:
            res.append((header, f"'{info[header]}'"))
        else:
            res.append((header, info[header]))
    return res

def constract_result(results: list, headers: list):
    res = []
    for index, result in enumerate(results):
        temp_res = {}
        for index, header in enumerate(headers):
            temp_res[header] = result[index]
        res.append(temp_res)
    return res

__all__ = [select_by_ids, select_all, update_single, add_single, delete_by_list, concat_add_info, hide_some_key, update_str_constractor, constract_result]
