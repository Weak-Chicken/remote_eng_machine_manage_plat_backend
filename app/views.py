from app import app
from app import mysql
from flask import request, render_template
import requests

@app.route('/test_conn', methods=['GET', 'POST'])
def test_conn():
    return "<h1>Flask is working</h1>"

@app.route('/test_db', methods=['GET', 'POST'])
def test_db():
    cursor = mysql.connection.cursor()
    sql = "select * from test_tb"
    try:
        cursor.execute(sql)
        # 获取所有记录列表
        results = cursor.fetchall()
        return str(results)
    except:
        return "Error"

@app.route('/', methods=['GET', 'POST'])
def index():
    cur = mysql.connection.cursor()
    result = cur.execute("select * from test_tb where name in ('1', '2', '3')")
    mysql.connection.commit()
    cur.close()
    return str(result)
    # if request.method == "POST":
    #     details = request.form
    #     firstName = details['fname']
    #     lastName = details['lname']
    #     cur = mysql.connection.cursor()
    #     cur.execute("INSERT INTO MyUsers(firstName, lastName) VALUES (%s, %s)", (firstName, lastName))
    #     mysql.connection.commit()
    #     cur.close()
    #     return 'success'
    # return render_template('index.html')